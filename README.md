# AnimalTraps

Plugin to capture, transport and release mobs with special animal traps.

## Download

Download the latest version from [here](https://gitlab.com/retrixe/animaltraps/-/releases) and place in the `plugins/` folder.

The plugin supports 1.14-1.16 only, create an issue if you would like to see an update. Use `1.0.0` for 1.12 support and `1.1.x` for 1.13 support.

## Usage

Run `/at-give (optionally, user)` to get an animal trap. You can also let players buy traps with `/buytrap` (edit price in `config.yml`, enabled by default).

Right-click animals to capture, right-click a block to release an animal from a trap.

For custom traps, you can use `/give` with the required format depending on the animal. 
