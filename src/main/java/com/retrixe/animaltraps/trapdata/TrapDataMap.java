package com.retrixe.animaltraps.trapdata;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TrapDataMap {
    private final HashMap<String, String> attributes = new LinkedHashMap<>();
    private EntityType entityType;

    public TrapDataMap() {}

    public TrapDataMap(EntityType entityType) {
        this.entityType = entityType;
    }

    public static TrapDataMap readFromLore(List<String> loreList) {
        List<String> lore = new ArrayList<>(loreList);
        TrapDataMap map = new TrapDataMap();
        // Remove the description.
        lore.remove(lore.size() - 1);
        lore.remove(lore.size() - 1);
        // Get the first line.
        String entityTypeLine = ChatColor.stripColor(lore.remove(0));
        String entityType = entityTypeLine.substring(entityTypeLine.indexOf(" ") + 1).replaceAll(" ", "_");
        if (entityType.equals("SNOW_GOLEM")) entityType = "SNOWMAN";
        else if (entityType.equals("MOOSHROOM")) entityType = "MUSHROOM_COW";
        map.setEntityType(EntityType.valueOf(entityType));
        for (String attr : lore) {
            String cleanAttr = ChatColor.stripColor(attr);
            int separatorIndex = cleanAttr.indexOf(":");
            map.attributes.put(cleanAttr.substring(0, separatorIndex), cleanAttr.substring(separatorIndex + 2));
        }
        return map;
    }

    public String getEntityTypeAsString() {
        if (entityType == EntityType.SNOWMAN) return "Snow golem";
        if (entityType == EntityType.MUSHROOM_COW) return "Mooshroom";
        return String.join(" ",
                Arrays.stream(entityType.toString().split("_"))
                        .map(s -> s.charAt(0) + s.substring(1).toLowerCase())
                        .toArray(String[]::new)
        );
    }

    public String toString(boolean verbose) {
        if (verbose) {
            StringBuilder string = new StringBuilder(getEntityTypeAsString());
            if (!attributes.isEmpty()) {
                string.append(" (");
                for (Map.Entry<String, String> entry : attributes.entrySet()) {
                    string.append(entry.getKey()).append(": ").append(entry.getValue()).append(", ");
                }
                string.replace(string.length() - 2, string.length(), ")");
            }
            return string.toString();
        }
        return this.toString();
    }

    public void log(Location location, String player, String captured) {
        String name = (location.getWorld() == null) ? "unknown world" : location.getWorld().getName();
        double x = Math.round(location.getX() * 10.0) / 10.0;
        double y = Math.round(location.getY() * 10.0) / 10.0;
        double z = Math.round(location.getZ() * 10.0) / 10.0;
        String loc = name + ' ' + x + ' ' + y + ' ' + z;
        String animal = this.toString(true);
        Bukkit.getLogger().info("[AnimalTraps] " + animal + " " + captured + " at " + loc + " by " + player);
    }

    public String getDisplayName() {
        return "§bAnimal Trap §fwith §a" + getEntityTypeAsString().toUpperCase();
    }

    public List<String> toLore() {
        ArrayList<String> lore = new ArrayList<>();
        lore.add("§aContains " + getEntityTypeAsString().toUpperCase());
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            lore.add("§a§o" + entry.getKey() + ": §f" + entry.getValue());
        }
        lore.add("");
        lore.add("§eRight-click to release.");
        return lore;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String get(String key) {
        return attributes.get(key);
    }

    public void set(String key, String value) {
        attributes.put(key, value);
    }

    public boolean getBoolean(String key) {
        return attributes.getOrDefault(key, "").equals("Yes");
    }

    public void setBoolean(String key, boolean value) {
        attributes.put(key, value ? "Yes" : "No");
    }

    public int getInt(String key) {
        return Integer.parseInt(attributes.get(key));
    }

    public void setInt(String key, int value) {
        attributes.put(key, Integer.toString(value));
    }

    public double getDouble(String key) {
        return Double.parseDouble(attributes.get(key));
    }

    public void setDouble(String key, double value) {
        attributes.put(key, Double.toString(value));
    }
}
