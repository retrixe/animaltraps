package com.retrixe.animaltraps.trapdata;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class TrapVersionManager {
    private final NamespacedKey key;
    public TrapVersionManager(Plugin plugin) {
        key = new NamespacedKey(plugin, "trap-version");
    }

    /*
    Trap versions:
    - N/A - Assumed to be Minecraft 1.12, this is *usually* safe for newer traps without version NBT attached.
    - 1.12-1 - Minecraft 1.12
    - 1.13-1 - Minecraft 1.13
    - 1.16-1 - Minecraft 1.14-1.18
    */
    public String getTrapVersion(ItemMeta itemMeta) {
        // Get the version used by the item.
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        String version = null;
        if (dataContainer.has(key, PersistentDataType.STRING)) {
            version = dataContainer.get(key, PersistentDataType.STRING);
        }
        return version;
    }

    public ItemMeta getUpdatedItemMeta(ItemMeta itemMeta) {
        String version = getTrapVersion(itemMeta);
        // Version updates.
        if (version == null) version = "1.12-1"; // Assume the version to be 1.12-1 if tag is not present.
        // 1.12-1 -> 1.13-1 trap-version update.
        if (version.equals("1.12-1")) {
            // Convert the trap to 1.13-1.
            List<String> lore = itemMeta.getLore();
            if (lore != null && lore.get(0).endsWith("WOLF") && lore.get(3).equals("§a§oCollar: §fSILVER"))
                lore.set(3, "§a§oCollar: §fLIGHT_GRAY");
            else if (lore != null && lore.get(0).endsWith("SHEEP") && lore.get(2).equals("§a§oColor: §fSILVER"))
                lore.set(2, "§a§oColor: §fLIGHT_GRAY");
            itemMeta.setLore(lore);
            version = "1.13-1";
        }
        // 1.13-1 -> 1.16-1 trap-version update (includes 1.14.4 and 1.15.2).
        if (version.equals("1.13-1")) {
            // Convert the trap to 1.16-1.
            List<String> lore = itemMeta.getLore();
            if (lore != null && lore.get(0).endsWith("MOOSHROOM") && lore.size() == 4) // Mooshrooms have variants now.
                lore.add(2, "§a§oVariant: §fRED");
            else if (lore != null && lore.get(0).endsWith("OCELOT") && lore.get(2).equals("§a§oTamed: §fYes")) {
                // Convert the entity type to CAT.
                lore.set(0, "§aContains CAT");

                // Add the collar color.
                lore.add(4, "§a§oCollar: §fRED");

                // Convert the cat type.
                if ("§a§oType: §fSIAMESE_CAT".equals(lore.get(3)))
                    lore.set(3, "§a§oType: §fSIAMESE");
                else if ("§a§oType: §fBLACK_CAT".equals(lore.get(3)))
                    lore.set(3, "§a§oType: §fBLACK");
                else if ("§a§oType: §fRED_CAT".equals(lore.get(3)))
                    lore.set(3, "§a§oType: §fRED");
            } else if (lore != null && lore.get(0).endsWith("OCELOT") && lore.get(2).equals("§a§oTamed: §fNo")) {
                // Convert Tamed property to Trusting.
                lore.set(2, "§a§oTrusting: §fNo");
                // Remove the ocelot type.
                lore.remove(3);
            }
            itemMeta.setLore(lore);
            version = "1.16-1";
        }

        // Update the trap-version NBT tag.
        itemMeta.getPersistentDataContainer().set(key, PersistentDataType.STRING, version);
        // Return the updated ItemMeta.
        return itemMeta;
    }
}
