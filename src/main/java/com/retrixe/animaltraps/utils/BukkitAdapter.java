package com.retrixe.animaltraps.utils;

import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Fox;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class BukkitAdapter {
    public static List<AnimalTamer> getFoxTrustedPlayers(Plugin plugin, Fox fox) { // Supports 1.14.4 and 1.15.2+ only.
        try {
            ArrayList<AnimalTamer> trusted = new ArrayList<>();
            trusted.add(fox.getFirstTrustedPlayer());
            trusted.add(fox.getSecondTrustedPlayer());
            return trusted;
        } catch (NoSuchMethodError ignored) {
            try { // Fallback for 1.14.4.
                Object entityFox = fox.getClass().getMethod("getHandle").invoke(fox);
                Method method = entityFox.getClass().getDeclaredMethod("ek");
                method.setAccessible(true);
                @SuppressWarnings("unchecked") List<UUID> trustedUuids = (List<UUID>) method.invoke(entityFox);
                ArrayList<AnimalTamer> players = new ArrayList<>();
                for (UUID uuid : trustedUuids) {
                    if (uuid == null) continue;
                    Player player = plugin.getServer().getPlayer(uuid);
                    players.add(player != null ? player : plugin.getServer().getOfflinePlayer(uuid));
                }
                return players;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static void setFoxTrustedPlayer(Fox fox, AnimalTamer player) { // Supports 1.14.4 and 1.15.2+ only.
        try {
            fox.setFirstTrustedPlayer(player);
        } catch (NoSuchMethodError ignored) { // Fallback for 1.14.4.
            try {
                Object entityFox = fox.getClass().getMethod("getHandle").invoke(fox);
                Object dataWatcher = entityFox.getClass().getMethod("getDataWatcher").invoke(entityFox);
                Field dataWatcherObjectField = entityFox.getClass().getDeclaredField("bB");
                dataWatcherObjectField.setAccessible(true);
                dataWatcher.getClass().getMethod("set", dataWatcherObjectField.getType(), Object.class)
                        .invoke(dataWatcher, dataWatcherObjectField.get(null), Optional.of(player.getUniqueId()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isOcelotTrusting(Ocelot ocelot) {
        try {
            Object entityOcelot = ocelot.getClass().getMethod("getHandle").invoke(ocelot);
            Method method = entityOcelot.getClass().getDeclaredMethod("isTrusting");
            method.setAccessible(true);
            return (boolean) method.invoke(entityOcelot);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void setOcelotTrusting(Ocelot ocelot) {
        try {
            Object entityOcelot = ocelot.getClass().getMethod("getHandle").invoke(ocelot);
            Method method = entityOcelot.getClass().getDeclaredMethod("setTrusting", boolean.class);
            method.setAccessible(true);
            method.invoke(entityOcelot, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
