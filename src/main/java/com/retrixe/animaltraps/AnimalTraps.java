package com.retrixe.animaltraps;

import com.retrixe.animaltraps.listeners.RightClickCaptureListener;
import com.retrixe.animaltraps.listeners.RightClickReleaseListener;
import com.retrixe.animaltraps.trapdata.TrapVersionManager;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand; // wtf NPEs
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

public class AnimalTraps extends JavaPlugin {
    private static AnimalTraps instance;
    public static AnimalTraps getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable () {
        try {
            saveDefaultConfig(); // No need to reload atm.
            ArrayList<String> cooldown = new ArrayList<>(); // Protect against instant release.
            TrapVersionManager versionManager = new TrapVersionManager(this);
            // Register event for capturing.
            this.getServer().getPluginManager().registerEvents(new RightClickCaptureListener(this, cooldown), this);
            // Register event for releasing.
            this.getServer().getPluginManager().registerEvents(new RightClickReleaseListener(versionManager, cooldown), this);

            // /animaltraps command.
            PluginCommand animaltrapsCommand = this.getCommand("animaltraps"); // wtf NPEs
            if (animaltrapsCommand != null) {
                animaltrapsCommand.setExecutor((commandSender, command, label, args) -> {
                    if (args.length != 1) return false;
                    else if (args[0].equals("reload")) {
                        if (commandSender.hasPermission("animaltraps.reload")) {
                            reloadConfig();
                            commandSender.sendMessage("§a[AnimalTraps] §bAnimalTraps plugin reloaded successfully.");
                        } else commandSender.sendMessage("§cYou do not have permission to run this command.");
                        return true;
                    } else if (args[0].equals("info")) {
                        if (!(commandSender instanceof Player)) {
                            commandSender.sendMessage("§a[AnimalTraps] §bOnly players may run this command!");
                        } else if (commandSender.hasPermission("animaltraps.info")) {
                            ItemStack trap = ((Player)commandSender).getInventory().getItemInMainHand();
                            ItemMeta itemMeta = trap.getItemMeta();
                            if (itemMeta != null && itemMeta.getLore() != null &&
                                    itemMeta.getLore().size() >= 3 && !itemMeta.getLore().get(0).equals("§aEMPTY") &&
                                    trap.getType() == Material.SLIME_BALL) {
                                // Send trap version.
                                String version = versionManager.getTrapVersion(itemMeta);
                                if (version == null)
                                    version = "N/A (Assumed to be 1.12-1)";
                                commandSender.sendMessage("§a[AnimalTraps] §bTrap version: §3" + version);
                                // Parse the updated trap and send its data.
                                List<String> lore = versionManager.getUpdatedItemMeta(itemMeta).getLore();
                                if (lore == null) throw new RuntimeException("Lore from VersionManager is null");
                                lore.remove(lore.size() - 1);
                                lore.remove(lore.size() - 1);
                                lore.forEach(m -> commandSender.sendMessage(m.replaceAll("§k", "")));
                            } else commandSender.sendMessage("§a[AnimalTraps] §bYou must have a trapped animal in" +
                                    " your hand to get information of!");
                        } else commandSender.sendMessage("§cYou do not have permission to run this command.");
                        return true;
                    } else return false;
                });
            }

            // /at-give command.
            PluginCommand atGive = this.getCommand("at-give"); // wtf NPEs
            if (atGive != null) {
                atGive.setExecutor((commandSender, command, label, args) -> atGiveExecutor(commandSender, args));
            }

            // /buytrap command.
            PluginCommand buytrap = this.getCommand("buytrap"); // wtf NPEs
            if (buytrap != null) {
                buytrap.setExecutor((commandSender, command, label, args) -> buytrapExecutor(commandSender, args));
                buytrap.setTabCompleter((sender, command, alias, args) -> {
                    if (args.length != 0 && args.length != 1) return new ArrayList<>();
                    ArrayList<String> list = new ArrayList<>();
                    list.add("confirm");
                    return list;
                });
            }
        } catch (Exception e) {
            this.getLogger().log(Level.SEVERE, "Unable to initialize plugin!", e);
        }
    }

    private boolean buytrapExecutor(CommandSender sender, String[] args) {
        if (sender.equals(this.getServer().getConsoleSender()) || !(sender instanceof Player)) {
            sender.sendMessage("[AnimalTraps] This command cannot be executed from the console!");
            return true;
        } else if (args.length != 0 && args.length != 1) return false;

        Player player = (Player)sender;
        double price = getConfig().getDouble("trapPrice", 2000);
        if (args.length == 0 || !args[0].equals("confirm")) {
            player.sendMessage("§a[AnimalTraps] §bAn animal trap costs $" + price +
                    ", run §a/buytrap confirm§b to purchase.");
            return true;
        } else if (player.getInventory().firstEmpty() == -1) {
            player.sendMessage("§a[AnimalTraps] §cYour inventory is full! The animal trap cannot be given.");
            return true;
        }

        // Take the money.
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            player.sendMessage("§a[AnimalTraps] §cThis command requires the Vault plugin with an economy provider!");
            return true;
        }
        Class<?> economyClass;
        try {
            economyClass = Class.forName("net.milkbowl.vault.economy.Economy");
            RegisteredServiceProvider<?> rsp = getServer().getServicesManager().getRegistration(economyClass);
            if (rsp == null) {
                player.sendMessage("§a[AnimalTraps] §cThis command requires the Vault plugin with an economy provider!");
                return true;
            }
            Object economy = rsp.getProvider();
            boolean has = (boolean) economyClass.getMethod("has", OfflinePlayer.class, double.class)
                    .invoke(economy, player, price);
            if (!has) {
                player.sendMessage("§a[AnimalTraps] §cAn animal trap costs $" + price + "! You do not have enough money.");
                return true;
            }
            Object resp = economyClass.getMethod("withdrawPlayer", OfflinePlayer.class, double.class)
                    .invoke(economy, player, price);
            if (!((boolean) resp.getClass().getMethod("transactionSuccess").invoke(resp))) {
                player.sendMessage("§a[AnimalTraps] §cFailed to withdraw money from your account.");
                return true;
            }
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            player.sendMessage("§a[AnimalTraps] §cThis command requires the Vault plugin with an economy provider!");
            return true;
        }
        player.getInventory().addItem(getEmptyAnimalTraps(1));
        sender.sendMessage("§a[AnimalTraps] §bAnimal trap given to you.");
        return true;
    }

    private boolean atGiveExecutor(CommandSender commandSender, String[] args) {
        if (args.length > 2) return false;
        else if (args.length == 0 && (
                commandSender.equals(this.getServer().getConsoleSender()) || !(commandSender instanceof Player)
        )) {
            commandSender.sendMessage("[AnimalTraps] This command cannot be executed from the console!");
            return true;
        }
        Player player;
        int amount = 1;
        if (args.length == 2) {
            try {
                amount = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        if (args.length >= 1) {
            player = this.getServer().getPlayer(args[0]);
            if (player == null) {
                commandSender.sendMessage("§a[AnimalTraps] §cThis player does not exist!");
                return true;
            }
        } else player = (Player)commandSender;
        String their = player.equals(commandSender) ? "Your" : "Their";
        String them = player.equals(commandSender) ? "you" : "them";
        HashMap<Integer, ItemStack> failure = player.getInventory().addItem(getEmptyAnimalTraps(amount));
        if (failure.size() != 0) {
            int failedNum = failure.get(0).getAmount();
            player.sendMessage("§a[AnimalTraps] §c" + their + " inventory is full! " + failure.get(0).getAmount() +
                    " animal trap" + (failedNum == 1 ? "" : "s") + " could not be given.");
            return true;
        }
        commandSender.sendMessage("§a[AnimalTraps] §bAnimal trap given to " + them + ".");
        return true;
    }

    public static ItemStack getEmptyAnimalTraps(int amount) {
        ItemStack slimeBall = new ItemStack(Material.SLIME_BALL, amount);
        ArrayList<String> lore = new ArrayList<>();

        // Lore.
        lore.add("§aEMPTY");
        lore.add("");
        lore.add("§eRight-click an animal to use.");
        // lore.add(ChatColor.RED + "ASID: " + ChatColor.MAGIC + Math.random()); - Replaced by NBT data.

        ItemMeta slimeBallMeta = slimeBall.getItemMeta();
        if (slimeBallMeta == null) throw new RuntimeException(); // wtf NPEs
        slimeBallMeta.setDisplayName("§bEmpty Animal Trap");
        slimeBallMeta.setLore(lore);
        /* slimeBallMeta.getPersistentDataContainer().set( // Add a unique ID to prevent stacking of these balls.
                new NamespacedKey(this, "asid"),
                PersistentDataType.STRING, Double.toString(Math.random())
        ); */
        slimeBall.setItemMeta(slimeBallMeta);
        return slimeBall;
    }
}
