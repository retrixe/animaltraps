package com.retrixe.animaltraps.listeners;

import com.retrixe.animaltraps.AnimalTraps;
import com.retrixe.animaltraps.trapdata.TrapDataMap;
import com.retrixe.animaltraps.trapdata.TrapVersionManager;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance; // wtf NPEs
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

import static com.retrixe.animaltraps.utils.BukkitAdapter.setFoxTrustedPlayer;
import static com.retrixe.animaltraps.utils.BukkitAdapter.setOcelotTrusting;

public class RightClickReleaseListener implements Listener {
    private final ArrayList<String> releaseCooldown = new ArrayList<>();
    private final List<String> cooldown;
    private final TrapVersionManager vm;
    public RightClickReleaseListener(TrapVersionManager versionManager, List<String> cooldown) {
        this.cooldown = cooldown;
        this.vm = versionManager;
    }

    private Location centreXZ(Location location) {
        return new Location(location.getWorld(), location.getX() + 0.5, location.getY(), location.getZ() + 0.5);
    }

    private Location upY(Location location) {
        return new Location(location.getWorld(), location.getX(), location.getY() + 1, location.getZ());
    }

    @EventHandler
    void onPlayerInteractEvent(PlayerInteractEvent e) {
        if (e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.SLIME_BALL) &&
            e.getPlayer().getInventory().getItemInMainHand().hasItemMeta() && (
                e.getPlayer().getInventory().getItemInMainHand().getItemMeta() != null && // wtf NPEs
                e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore() != null &&
                e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore().size() >= 3 &&
                e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore().get(0) != null &&
                !e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore().get(0).endsWith("EMPTY")
            ) && e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getHand() == EquipmentSlot.OFF_HAND &&
            e.getClickedBlock() != null && e.getPlayer().hasPermission("AnimalTraps.release") &&
            !cooldown.contains(e.getPlayer().getName()) &&
            !releaseCooldown.contains(e.getPlayer().getName())
        ) {
            // Prevent stuff.
            Location spawnLocation = upY(centreXZ(e.getClickedBlock().getLocation()));
            if (!spawnLocation.getBlock().isPassable() || !upY(spawnLocation).getBlock().isPassable()) {
                e.getPlayer().sendMessage("§bMiniumum 2 blocks of clearance is required to release a pet!");
                return;
            }
            // A lot of animals e_e
            ItemMeta itemMeta = vm.getUpdatedItemMeta(e.getPlayer().getInventory().getItemInMainHand().getItemMeta());
            List<String> lore = itemMeta.getLore();
            if (lore == null) return;
            TrapDataMap trapData = TrapDataMap.readFromLore(lore);
            EntityType et = trapData.getEntityType();
            if (et == EntityType.SNOWMAN) this.releaseLivingEntity(e, trapData);
            else if (et == EntityType.IRON_GOLEM) this.releaseLivingEntity(e, trapData);
            else if (et == EntityType.SQUID) this.releaseLivingEntity(e, trapData);
            else if (et.toString().equals("GLOW_SQUID")) this.releaseLivingEntity(e, trapData);
            else if (et == EntityType.CHICKEN) this.releaseAgeable(e, trapData);
            else if (et == EntityType.POLAR_BEAR) this.releaseAgeable(e, trapData);
            else if (et == EntityType.COW) this.releaseAgeable(e, trapData);
            else if (et == EntityType.TURTLE) this.releaseAgeable(e, trapData);
            else if (et.toString().equals("BEE")) this.releaseAgeable(e, trapData);
            else if (et == EntityType.MUSHROOM_COW) this.releaseMooshroom(e, trapData);
            else if (et.toString().equals("AXOLOTL")) this.releaseAxolotl(e, trapData);
            else if (et.toString().equals("GOAT")) this.releaseGoat(e, trapData);
            else if (et == EntityType.SHEEP) this.releaseSheep(e, trapData);
            else if (et == EntityType.RABBIT) this.releaseRabbit(e, trapData);
            else if (et == EntityType.PANDA) this.releasePanda(e, trapData);
            else if (et == EntityType.PIG) this.releasePig(e, trapData);
            else if (et.toString().equals("STRIDER")) this.releaseStrider(e, trapData);
            else if (et == EntityType.FOX) this.releaseFox(e, trapData);
            else if (et == EntityType.WOLF) this.releaseWolf(e, trapData);
            else if (et == EntityType.CAT) this.releaseCat(e, trapData);
            else if (et == EntityType.OCELOT) this.releaseOcelot(e, trapData);
            else if (et == EntityType.PARROT) this.releaseParrot(e, trapData);
            else if (et == EntityType.LLAMA) this.releaseLlama(e, trapData);
            else if (et == EntityType.DONKEY) this.releaseDonkey(e, trapData);
            else if (et == EntityType.MULE) this.releaseMule(e, trapData);
            else if (et == EntityType.HORSE) this.releaseHorse(e, trapData);
            else return;
            trapData.log(upY(e.getPlayer().getLocation()), e.getPlayer().getName(), "released");
            e.getPlayer().getInventory().getItemInMainHand().setAmount(e.getPlayer().getInventory().getItemInMainHand().getAmount() - 1);
            e.getPlayer().getInventory().setItemInMainHand(e.getPlayer().getInventory().getItemInMainHand());
            // Put the user on release cooldown to prevent this event being fired twice for the same click.
            releaseCooldown.add(e.getPlayer().getName());
            e.getPlayer().getServer().getScheduler().runTaskLater(
                    AnimalTraps.getInstance(),
                    () -> releaseCooldown.remove(e.getPlayer().getName()), 5L
            );
        }
    }

    // TODO: Clean up functions to be more consistent and less redundant.
    private void releaseLivingEntity(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        // Delete item and spawn animal after logging.
        e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), data.getEntityType());
    }

    private void releaseAgeable(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), data.getEntityType());
        ((Ageable)spawnEntity).setAge(data.getBoolean("Baby") ? -24000 : 0);
    }

    private void releaseMooshroom(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        MushroomCow.Variant variant = MushroomCow.Variant.valueOf(data.get("Variant"));

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.MUSHROOM_COW);
        ((MushroomCow)spawnEntity).setAge(age);
        ((MushroomCow)spawnEntity).setVariant(variant);
    }

    private void releaseAxolotl(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.AXOLOTL);
        ((Axolotl)spawnEntity).setAge(data.getBoolean("Baby") ? -24000 : 0);
        ((Axolotl)spawnEntity).setVariant(Axolotl.Variant.valueOf(data.get("Variant")));
    }

    private void releaseGoat(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.GOAT);
        ((Goat)spawnEntity).setAge(data.getBoolean("Baby") ? -24000 : 0);
        ((Goat)spawnEntity).setScreaming(data.getBoolean("Screaming Goat"));
    }

    private void releaseSheep(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        DyeColor color = DyeColor.valueOf(data.get("Color"));
        boolean sheared = data.getBoolean("Sheared");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.SHEEP);
        ((Sheep)spawnEntity).setAge(age);
        ((Sheep)spawnEntity).setColor(color);
        ((Sheep)spawnEntity).setSheared(sheared);
    }

    private void releaseRabbit(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        Rabbit.Type rabbitType = Rabbit.Type.valueOf(data.get("Type"));

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.RABBIT);
        ((Rabbit)spawnEntity).setAge(age);
        ((Rabbit)spawnEntity).setRabbitType(rabbitType);
    }

    private void releasePanda(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        Panda.Gene mainGene = Panda.Gene.valueOf(data.get("Main Gene"));
        Panda.Gene hiddenGene = Panda.Gene.valueOf(data.get("Hidden Gene"));
        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.PANDA);
        ((Panda)spawnEntity).setAge(age);
        ((Panda)spawnEntity).setMainGene(mainGene);
        ((Panda)spawnEntity).setHiddenGene(hiddenGene);
    }

    private void releaseFox(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        Fox.Type type = Fox.Type.valueOf(data.get("Type"));
        boolean trusting = data.getBoolean("Trusting");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.FOX);
        ((Fox)spawnEntity).setAge(age);
        ((Fox)spawnEntity).setFoxType(type);
        if (trusting) setFoxTrustedPlayer((Fox) spawnEntity, e.getPlayer());
    }

    private void releaseWolf(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        boolean tamed = data.getBoolean("Tamed");
        DyeColor color = DyeColor.valueOf(data.get("Collar"));

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.WOLF);
        ((Wolf)spawnEntity).setAge(age);
        ((Wolf)spawnEntity).setCollarColor(color);
        if (tamed) ((Wolf)spawnEntity).setOwner(e.getPlayer());
    }

    private void releaseCat(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        boolean tamed = data.getBoolean("Tamed");
        Cat.Type catType = Cat.Type.valueOf(data.get("Type"));
        DyeColor color = DyeColor.valueOf(data.get("Collar"));

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.CAT);
        ((Cat)spawnEntity).setAge(age);
        ((Cat)spawnEntity).setCatType(catType);
        ((Cat)spawnEntity).setCollarColor(color);
        if (tamed) {
            ((Cat)spawnEntity).setOwner(e.getPlayer());
            ((Cat)spawnEntity).setTamed(true);
        }
    }

    private void releaseOcelot(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        boolean trusting = data.getBoolean("Trusting");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.OCELOT);
        ((Ocelot)spawnEntity).setAge(age);
        if (trusting) {
            setOcelotTrusting((Ocelot)spawnEntity);
            ((Ocelot)spawnEntity).setRemoveWhenFarAway(false);
        }
    }

    private void releasePig(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        boolean saddled = data.getBoolean("Saddled");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.PIG);
        ((Pig)spawnEntity).setAge(age);
        ((Pig)spawnEntity).setSaddle(saddled);
    }

    private void releaseStrider(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        int age = data.getBoolean("Baby") ? -24000 : 0;
        boolean saddled = data.getBoolean("Saddled");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.STRIDER);
        ((Strider)spawnEntity).setAge(age);
        ((Strider)spawnEntity).setSaddle(saddled);
    }

    private void releaseParrot(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        boolean tamed = data.getBoolean("Tamed");
        Parrot.Variant parrotVariant = Parrot.Variant.valueOf(data.get("Type"));

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.PARROT);
        ((Parrot)spawnEntity).setVariant(parrotVariant);
        if (tamed) ((Parrot)spawnEntity).setOwner(e.getPlayer());
    }

    private void releaseLlama(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        boolean baby = data.getBoolean("Baby");
        boolean tamed = data.getBoolean("Tamed");
        boolean carryingChest = data.getBoolean("Chested");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.LLAMA);
        ((Llama)spawnEntity).setAge(baby ? -24000 : 0);
        ((Llama)spawnEntity).setTamed(tamed);
        ((Llama)spawnEntity).setCarryingChest(carryingChest);
        ((Llama)spawnEntity).setColor(Llama.Color.valueOf(data.get("Color")));
        ((Llama)spawnEntity).setStrength(data.getInt("Strength"));
        AttributeInstance maxHealth = ((Llama)spawnEntity).getAttribute(Attribute.GENERIC_MAX_HEALTH);
        if (maxHealth == null) return; // wtf NPEs
        maxHealth.setBaseValue(data.getDouble("Max Health"));
    }

    private void releaseDonkey(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        boolean baby = data.getBoolean("Baby");
        boolean saddled = data.getBoolean("Saddled");
        boolean tamed = data.getBoolean("Tamed");
        boolean chested = data.getBoolean("Chested");
        double maxHealthValue = data.getDouble("Max Health");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.DONKEY);
        ((Donkey)spawnEntity).setAge(baby ? -24000 : 0);
        ((Donkey)spawnEntity).getInventory().setSaddle(saddled ? new ItemStack(Material.SADDLE) : null);
        ((Donkey)spawnEntity).setTamed(tamed);
        ((Donkey)spawnEntity).setCarryingChest(chested);
        AttributeInstance maxHealth = ((Donkey)spawnEntity).getAttribute(Attribute.GENERIC_MAX_HEALTH);
        if (maxHealth == null) return; // wtf NPEs
        maxHealth.setBaseValue(maxHealthValue);
    }

    private void releaseMule(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        boolean baby = data.getBoolean("Baby");
        boolean saddled = data.getBoolean("Saddled");
        boolean tamed = data.getBoolean("Tamed");
        boolean chested = data.getBoolean("Chested");
        double jumpStrengthValue = data.getDouble("Jump Strength");
        double maxHealthValue = data.getDouble("Max Health");
        double speedValue = data.getDouble("Speed");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.MULE);
        ((Mule)spawnEntity).setAge(baby ? -24000 : 0);
        ((Mule)spawnEntity).getInventory().setSaddle(saddled ? new ItemStack(Material.SADDLE) : null);
        ((Mule)spawnEntity).setTamed(tamed);
        ((Mule)spawnEntity).setCarryingChest(chested);
        ((Mule)spawnEntity).setJumpStrength(jumpStrengthValue);
        AttributeInstance maxHealth = ((Mule)spawnEntity).getAttribute(Attribute.GENERIC_MAX_HEALTH); // wtf NPEs
        AttributeInstance movementSpeed = ((Mule)spawnEntity).getAttribute(Attribute.GENERIC_MOVEMENT_SPEED); // wtf NPEs
        if (maxHealth == null || movementSpeed == null) return; // wtf NPEs
        maxHealth.setBaseValue(maxHealthValue);
        movementSpeed.setBaseValue(speedValue);
    }

    private void releaseHorse(PlayerInteractEvent e, TrapDataMap data) {
        if (e.getClickedBlock() == null) return; // wtf NPEs
        boolean baby = data.getBoolean("Baby");
        boolean saddled = data.getBoolean("Saddled");
        boolean tamed = data.getBoolean("Tamed");
        String color = data.get("Color");
        String style = data.get("Style");
        double jumpStrengthValue = data.getDouble("Jump Strength");
        double maxHealthValue = data.getDouble("Max Health");
        double speedValue = data.getDouble("Speed");

        // Delete item and spawn animal after logging.
        Entity spawnEntity = e.getPlayer().getWorld().spawnEntity(this.upY(e.getClickedBlock().getLocation()), EntityType.HORSE);
        ((Horse)spawnEntity).setAge(baby ? -24000 : 0);
        ((Horse)spawnEntity).getInventory().setSaddle(saddled ? new ItemStack(Material.SADDLE) : null);
        ((Horse)spawnEntity).setTamed(tamed);
        ((Horse)spawnEntity).setColor(Horse.Color.valueOf(color));
        ((Horse)spawnEntity).setStyle(Horse.Style.valueOf(style));
        ((Horse)spawnEntity).setJumpStrength(jumpStrengthValue);
        AttributeInstance maxHealth = ((Horse)spawnEntity).getAttribute(Attribute.GENERIC_MAX_HEALTH); // wtf NPEs
        AttributeInstance movementSpeed = ((Horse)spawnEntity).getAttribute(Attribute.GENERIC_MOVEMENT_SPEED); // wtf NPEs
        if (maxHealth == null || movementSpeed == null) return; // wtf NPEs
        maxHealth.setBaseValue(maxHealthValue);
        movementSpeed.setBaseValue(speedValue);
    }
}
