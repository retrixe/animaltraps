package com.retrixe.animaltraps.listeners;

import com.retrixe.animaltraps.AnimalTraps;
import com.retrixe.animaltraps.trapdata.TrapDataMap;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance; // wtf NPEs
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.retrixe.animaltraps.utils.BukkitAdapter.getFoxTrustedPlayers;
import static com.retrixe.animaltraps.utils.BukkitAdapter.isOcelotTrusting;

public class RightClickCaptureListener implements Listener {
    private final ArrayList<String> captureCooldown = new ArrayList<>();
    private final ArrayList<String> cooldown;
    private final AnimalTraps plugin; // wtf NPEs but ok
    public RightClickCaptureListener(AnimalTraps plugin, ArrayList<String> cooldown) {
        this.cooldown = cooldown;
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        Entity entity = e.getRightClicked();
        ItemStack emptyTrap = player.getInventory().getItemInMainHand();
        ItemMeta itemMeta = emptyTrap.getItemMeta();

        if (itemMeta != null &&
                itemMeta.getLore() != null &&
                itemMeta.getLore().size() >= 3 &&
                itemMeta.getLore().get(0).equals("§aEMPTY") &&
                emptyTrap.getType() == Material.SLIME_BALL &&
                player.hasPermission("AnimalTraps.capture") &&
                !captureCooldown.contains(player.getName())
        ) {
            // Prevent stuff.
            e.setCancelled(true);
            // A lot of animals e_e
            TrapDataMap trapData;
            if (entity.getType() == EntityType.SNOWMAN) trapData = new TrapDataMap(entity.getType());
            else if (entity.getType() == EntityType.IRON_GOLEM) trapData = new TrapDataMap(entity.getType());
            else if (entity.getType() == EntityType.SQUID) trapData = new TrapDataMap(entity.getType());
            else if (entity.getType().toString().equals("GLOW_SQUID")) trapData = new TrapDataMap(entity.getType());
            else if (entity.getType() == EntityType.CHICKEN) trapData = captureAgeable(entity);
            else if (entity.getType() == EntityType.POLAR_BEAR) trapData = captureAgeable(entity);
            else if (entity.getType() == EntityType.COW) trapData = captureAgeable(entity);
            else if (entity.getType() == EntityType.TURTLE) trapData = captureAgeable(entity);
            else if (entity.getType().toString().equals("BEE") && ((Bee) entity).getAnger() == 0) {
                trapData = captureAgeable(entity);
            } else if (entity.getType() == EntityType.MUSHROOM_COW) trapData = captureMooshroom(entity);
            else if (entity.getType().toString().equals("AXOLOTL")) trapData = captureAxolotl(entity);
            else if (entity.getType().toString().equals("GOAT")) trapData = captureGoat(entity);
            else if (entity.getType() == EntityType.SHEEP) trapData = captureSheep(entity);
            else if (entity.getType() == EntityType.RABBIT) trapData = captureRabbit(entity);
            else if (entity.getType() == EntityType.PANDA) trapData = capturePanda(entity);
            else if (entity.getType() == EntityType.PIG) trapData = capturePig(entity);
            else if (entity.getType().toString().equals("STRIDER")) trapData = captureStrider(entity);
            else if (entity.getType() == EntityType.FOX) trapData = captureFox(entity, player);
            else if (entity.getType() == EntityType.WOLF) trapData = captureWolf(entity, player);
            else if (entity.getType() == EntityType.CAT) trapData = captureCat(entity, player);
            else if (entity.getType() == EntityType.OCELOT) trapData = captureOcelot(entity);
            else if (entity.getType() == EntityType.PARROT) trapData = captureParrot(entity, player);
            else if (entity.getType() == EntityType.LLAMA) trapData = captureLlama(entity, player);
            else if (entity.getType() == EntityType.DONKEY) trapData = captureDonkey(entity, player);
            else if (entity.getType() == EntityType.MULE) trapData = captureMule(entity, player);
            else if (entity.getType() == EntityType.HORSE) trapData = captureHorse(entity, player);
            else return;
            if (trapData == null) return;
            // Set display name and lore to item.
            itemMeta.setDisplayName(trapData.getDisplayName());
            itemMeta.setLore(trapData.toLore());
            itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            itemMeta.getPersistentDataContainer().remove(new NamespacedKey(plugin, "asid")); // Not useful now.
            itemMeta.getPersistentDataContainer().set(
                    new NamespacedKey(plugin, "trap-version"), PersistentDataType.STRING, "1.16-1"
            );
            if (emptyTrap.getAmount() == 1) {
                emptyTrap.setItemMeta(itemMeta);
                emptyTrap.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, 1);
            } else {
                ItemStack newTrap = new ItemStack(Material.SLIME_BALL);
                newTrap.setItemMeta(itemMeta);
                newTrap.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, 1);
                HashMap<Integer, ItemStack> failure = e.getPlayer().getInventory().addItem(newTrap);
                if (failure.size() > 0) {
                    player.sendMessage("§a[AnimalTraps] §cYou do not have space in your inventory to capture this animal!");
                    return;
                } else emptyTrap.setAmount(emptyTrap.getAmount() - 1);
            }
            // Kill the animal.
            entity.remove();
            trapData.log(player.getLocation(), player.getName(), "captured");
            // Put the user on cooldown to prevent instant release.
            cooldown.add(player.getName());
            player.getServer().getScheduler().runTaskLater(plugin, () -> cooldown.remove(player.getName()), 40L);
            // Put the user on capture cooldown to prevent this event being fired twice for the same entity.
            captureCooldown.add(player.getName());
            player.getServer().getScheduler().runTaskLater(plugin, () -> captureCooldown.remove(player.getName()), 5L);
        }
    }

    private TrapDataMap captureAgeable(Entity entity) {
        TrapDataMap trapData = new TrapDataMap(entity.getType());
        trapData.setBoolean("Baby", ((Ageable)entity).getAge() < 0);
        return trapData;
    }

    private TrapDataMap captureMooshroom(Entity entity) {
        MushroomCow mushroomCow = (MushroomCow)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.MUSHROOM_COW);
        trapData.setBoolean("Baby", mushroomCow.getAge() < 0);
        trapData.set("Variant", mushroomCow.getVariant().toString());
        return trapData;
    }

    private TrapDataMap captureAxolotl(Entity entity) {
        Axolotl axolotl = (Axolotl)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.AXOLOTL);
        trapData.setBoolean("Baby", axolotl.getAge() < 0);
        trapData.set("Variant", axolotl.getVariant().toString());
        return trapData;
    }

    private TrapDataMap captureGoat(Entity entity) {
        Goat goat = (Goat)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.GOAT);
        trapData.setBoolean("Baby", goat.getAge() < 0);
        trapData.setBoolean("Screaming Goat", goat.isScreaming());
        return trapData;
    }

    private TrapDataMap captureSheep(Entity entity) {
        Sheep sheep = (Sheep)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.SHEEP);
        trapData.setBoolean("Baby", sheep.getAge() < 0);
        trapData.set("Color", sheep.getColor() == null ? "WHITE" : sheep.getColor().toString());
        trapData.setBoolean("Sheared", sheep.isSheared());
        return trapData;
    }

    private TrapDataMap captureRabbit(Entity entity) {
        Rabbit rabbit = (Rabbit)entity;
        if (rabbit.getRabbitType() == Rabbit.Type.THE_KILLER_BUNNY) return null;
        TrapDataMap trapData = new TrapDataMap(EntityType.RABBIT);
        trapData.setBoolean("Baby", rabbit.getAge() < 0);
        trapData.set("Type", rabbit.getRabbitType().toString());
        return trapData;
    }

    private TrapDataMap capturePanda(Entity entity) {
        Panda panda = (Panda)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.PANDA);
        trapData.setBoolean("Baby", panda.getAge() < 0);
        trapData.set("Main Gene", panda.getMainGene().toString());
        trapData.set("Hidden Gene", "§k" + panda.getHiddenGene());
        return trapData;
    }

    private TrapDataMap captureFox(Entity entity, Player player) {
        Fox fox = (Fox)entity;
        List<AnimalTamer> trustedPlayers = getFoxTrustedPlayers(plugin, fox);
        AnimalTamer firstPlayer = trustedPlayers != null && trustedPlayers.size() >= 1 ? trustedPlayers.get(0) : null;
        AnimalTamer secondPlayer = trustedPlayers != null && trustedPlayers.size() >= 2 ? trustedPlayers.get(1) : null;
        if (
                firstPlayer != null && !firstPlayer.getUniqueId().equals(player.getUniqueId())
                        && (secondPlayer == null || !secondPlayer.getUniqueId().equals(player.getUniqueId()))
        ) {
            player.sendMessage("§bThis fox is not your pet!");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.FOX);
        trapData.setBoolean("Baby", fox.getAge() < 0);
        trapData.set("Type", fox.getFoxType().toString());
        trapData.setBoolean("Trusting", firstPlayer != null || secondPlayer != null);
        return trapData;
    }

    private TrapDataMap captureWolf(Entity entity, Player player) {
        Wolf wolf = (Wolf)entity;
        if (wolf.isAngry()) return null;
        else if (wolf.getOwner() != null && !wolf.getOwner().getUniqueId().equals(player.getUniqueId())) {
            player.sendMessage("§bThis wolf is not your pet!");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.WOLF);
        trapData.setBoolean("Baby", wolf.getAge() < 0);
        trapData.setBoolean("Tamed", wolf.isTamed());
        trapData.set("Collar", wolf.getCollarColor().toString());
        return trapData;
    }

    private TrapDataMap captureCat(Entity entity, Player player) {
        Cat cat = (Cat)entity;
        if (cat.getOwner() != null && !cat.getOwner().getUniqueId().equals(player.getUniqueId())) {
            player.sendMessage("§bThis cat is not your pet!");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.CAT);
        trapData.setBoolean("Baby", cat.getAge() < 0);
        trapData.setBoolean("Tamed", cat.isTamed());
        trapData.set("Type", cat.getCatType().toString());
        trapData.set("Collar", cat.getCollarColor().toString());
        return trapData;
    }

    private TrapDataMap captureOcelot(Entity entity) {
        Ocelot ocelot = (Ocelot)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.OCELOT);
        trapData.setBoolean("Baby", ocelot.getAge() < 0);
        trapData.setBoolean("Trusting", isOcelotTrusting(ocelot));
        return trapData;
    }

    private TrapDataMap capturePig(Entity entity) {
        Pig pig = (Pig)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.PIG);
        trapData.setBoolean("Baby", pig.getAge() < 0);
        trapData.setBoolean("Saddled", pig.hasSaddle());
        return trapData;
    }

    private TrapDataMap captureStrider(Entity entity) {
        Strider strider = (Strider)entity;
        TrapDataMap trapData = new TrapDataMap(EntityType.STRIDER);
        trapData.setBoolean("Baby", strider.getAge() < 0);
        trapData.setBoolean("Saddled", strider.hasSaddle());
        return trapData;
    }

    private TrapDataMap captureParrot(Entity entity, Player player) {
        Parrot parrot = (Parrot)entity;
        if (parrot.getOwner() != null && !parrot.getOwner().getUniqueId().equals(player.getUniqueId())) {
            player.sendMessage("§bThis parrot is not your pet!");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.PARROT);
        trapData.setBoolean("Baby", parrot.getAge() < 0);
        trapData.set("Type", parrot.getVariant().toString());
        return trapData;
    }

    private TrapDataMap captureLlama(Entity entity, Player player) {
        Llama llama = (Llama)entity;
        boolean isEmpty = true;
        for (Object itemStack : llama.getInventory().getContents()) {
            if (itemStack != null) {
                isEmpty = false;
                break;
            }
        }
        if (llama.getInventory().getDecor() != null) {
            player.sendMessage("§bThis llama is equipped with a carpet! Remove the carpet to trap.");
            return null;
        } else if (llama.isCarryingChest() && !isEmpty) {
            player.sendMessage("§bThis llama is equipped with a chest containing items! Remove items to trap.");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.LLAMA);
        trapData.setBoolean("Baby", llama.getAge() < 0);
        trapData.setBoolean("Tamed", llama.isTamed());
        trapData.setBoolean("Chested", llama.isCarryingChest());
        trapData.set("Color", llama.getColor().toString());
        trapData.setInt("Strength", llama.getStrength());
        AttributeInstance maxHealth = llama.getAttribute(Attribute.GENERIC_MAX_HEALTH); // wtf NPEs
        if (maxHealth == null) return null; // wtf NPEs
        trapData.setDouble("Max Health", maxHealth.getValue());
        return trapData;
    }

    private TrapDataMap captureDonkey(Entity entity, Player player) {
        Donkey donkey = (Donkey)entity;
        boolean isEmpty = true; boolean first = true;
        for (Object itemStack : donkey.getInventory().getContents()) {
            if (first) first = false;
            else if (itemStack != null) isEmpty = false;
        }
        if (donkey.isCarryingChest() && !isEmpty) {
            player.sendMessage("§bThis donkey is equipped with a chest containing items! Remove items to trap.");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.DONKEY);
        trapData.setBoolean("Baby", donkey.getAge() < 0);
        trapData.setBoolean("Saddled", donkey.getInventory().getSaddle() != null);
        trapData.setBoolean("Tamed", donkey.isTamed());
        trapData.setBoolean("Chested", donkey.isCarryingChest());
        AttributeInstance maxHealth = donkey.getAttribute(Attribute.GENERIC_MAX_HEALTH); // wtf NPEs
        if (maxHealth == null) return null; // wtf NPEs
        trapData.setDouble("Max Health", maxHealth.getValue());
        return trapData;
    }

    private TrapDataMap captureMule(Entity entity, Player player) {
        Mule mule = (Mule)entity;
        boolean isEmpty = true; boolean first = true;
        for (Object itemStack : mule.getInventory().getContents()) {
            if (first) first = false;
            else if (itemStack != null) isEmpty = false;
        }
        if (mule.isCarryingChest() && !isEmpty) {
            player.sendMessage("§bThis mule is equipped with a chest containing items! Remove items to trap.");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.MULE);
        trapData.setBoolean("Baby", mule.getAge() < 0);
        trapData.setBoolean("Saddled", mule.getInventory().getSaddle() != null);
        trapData.setBoolean("Tamed", mule.isTamed());
        trapData.setBoolean("Chested", mule.isCarryingChest());
        trapData.setDouble("Jump Strength", mule.getJumpStrength());
        AttributeInstance maxHealth = mule.getAttribute(Attribute.GENERIC_MAX_HEALTH); // wtf NPEs
        AttributeInstance movementSpeed = mule.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED); // wtf NPEs
        if (maxHealth == null || movementSpeed == null) return null; // wtf NPEs
        trapData.setDouble("Max Health", maxHealth.getValue());
        trapData.setDouble("Speed", movementSpeed.getValue());
        return trapData;
    }

    private TrapDataMap captureHorse(Entity entity, Player player) {
        Horse horse = (Horse)entity;
        if (horse.getInventory().getArmor() != null) {
            player.sendMessage("§bThis horse is equipped with armor! Remove the armor to trap.");
            return null;
        }
        TrapDataMap trapData = new TrapDataMap(EntityType.HORSE);
        trapData.setBoolean("Baby", horse.getAge() < 0);
        trapData.setBoolean("Saddled", horse.getInventory().getSaddle() != null);
        trapData.setBoolean("Tamed", horse.isTamed());
        trapData.set("Color", horse.getColor().toString());
        trapData.set("Style", horse.getStyle().toString());
        trapData.setDouble("Jump Strength", horse.getJumpStrength());
        AttributeInstance maxHealth = horse.getAttribute(Attribute.GENERIC_MAX_HEALTH); // wtf NPEs
        AttributeInstance movementSpeed = horse.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED); // wtf NPEs
        if (maxHealth == null || movementSpeed == null) return null; // wtf NPEs
        trapData.setDouble("Max Health", maxHealth.getValue());
        trapData.setDouble("Speed", movementSpeed.getValue());
        return trapData;
    }
}
